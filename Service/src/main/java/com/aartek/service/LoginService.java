package com.aartek.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aartek.model.LoginDto;
import com.aartek.repository.LoginRepository;
import com.aartek.repository.LoginRepository;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepository;

	public LoginDto validate(LoginDto logindto) {
		  List<LoginDto> list= loginRepository.checkLogin(logindto);
		  if (list != null && !list.isEmpty()) {
		   return list.get(0);
		  } else {
		   return null;
		  }
		 }
}

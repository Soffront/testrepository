package com.aartek.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aartek.model.RegistrationDto;

@Repository
public class RegistrationRepository {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public RegistrationDto saveStudentDeatils(RegistrationDto registration) {

		if (registration != null) {

			hibernateTemplate.saveOrUpdate(registration);

			return registration;
		} else {
			return null;
		}
	}

}

package com.aartek.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.aartek.model.LoginDto;

@Repository
public class LoginRepository {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public List<LoginDto> checkLogin(LoginDto loginDto) {
		if (loginDto.getEmail_id() != null && loginDto.getPassword() != null) {
			System.out.println(loginDto.getEmail_id() + "     " + loginDto.getPassword());
			List<LoginDto> login = hibernateTemplate.find("from LoginDto al where al.email_id = ? and al.password = ?",loginDto.getEmail_id(), loginDto.getPassword());
			return login;
		} else {
			return null;
		}
	}
}

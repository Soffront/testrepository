package com.aartek.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aartek.model.LoginDto;
import com.aartek.service.LoginService;
@Controller
public class LoginController {

	@RequestMapping("/login")
	public String showLogin(Map<String, Object> map) {
		map.put("login", new LoginDto());
		return "login";
	}

	@Autowired
	LoginService loginService;
	
	@RequestMapping(value = "/userSignIn", method = RequestMethod.POST)
	public String signInAction(@ModelAttribute("login") LoginDto login) {

		  
			  LoginDto dto=loginService.validate(login);
			  if(dto!=null)
			  {
			   System.out.println("valid user");
			   return "welcome"; 
			  }
			  else
			  {
			   System.out.println("invalid user");
			   return "login";
			  }
			  
			 }
		
	}


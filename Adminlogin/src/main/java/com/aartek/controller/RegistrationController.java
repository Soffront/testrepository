package com.aartek.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aartek.model.RegistrationDto;
import com.aartek.service.RegistrationService;
import com.aartek.validator.RegistrationValidator;

@Controller
public class RegistrationController 
{


	@RequestMapping("/registration")
	public String showregistrationPage(Map<String, Object> map) {
		map.put("Registration", new RegistrationDto());
		return "registration";
	}

	@RequestMapping(value = "/saveStudentDetails", method = { RequestMethod.POST })
	public String saveStudentDeatils(@ModelAttribute("Registration") RegistrationDto registrationDto,BindingResult result) {
		boolean status = false;

		status = registrationService.saveStudentDeatils(registrationDto);
		
		registrationValidator.validate(registrationDto, result);
		if (result.hasErrors()) {
			return "registration";

		} else {
			return "welcome";

		}

	}

	@RequestMapping("/welcome")
	public String showWelcomePage(Map<String, Object> map, Model model) {
		return "welcome";
	}

}

package com.aartek.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator; 
import com.aartek.model.RegistrationDto;

@Component
public class RegistrationValidator implements Validator{

	public boolean supports(Class<?> clazz) {
		return RegistrationDto.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		
		RegistrationDto registrationDto=(RegistrationDto) target;
		
//		ValidationUtils.rejectIfEmpty(errors, "firstName", "Please Enter FirstName");
//		ValidationUtils.rejectIfEmpty(errors, "lastName", "Please Enter lastName");
//		ValidationUtils.rejectIfEmpty(errors, "emailId", "Please Enter emailId");
//		ValidationUtils.rejectIfEmpty(errors, "contact", "Please Enter contact");
//		ValidationUtils.rejectIfEmpty(errors, "password", "Please Enter password");

		ValidationUtils.rejectIfEmpty(errors, "lastName", "error.lastName.empty");
		ValidationUtils.rejectIfEmpty(errors, "emailId", "error.email.empty");
		ValidationUtils.rejectIfEmpty(errors, "contact", "error.contact.empty");
		ValidationUtils.rejectIfEmpty(errors, "password", "error.password.empty");

		if (registrationDto.getFirstName()!= "" && registrationDto.getFirstName().matches("[a-zA-Z]{2,25}") != true){
			errors.rejectValue("firstName", "error.firstname.first.rule");
		}
		else{ 
			 
			ValidationUtils.rejectIfEmpty(errors, "firstName", "error.firstName.empty");

		}
		
		if (registrationDto.getLastName().matches("[a-zA-Z]{2,25}") != true) {
			errors.rejectValue("lastName", "error.lastname.first.rule");
		}
		if (registrationDto.getEmailId().contains("@gmail.com") != true) {
			errors.rejectValue("emailId", "error.email.first.rule");
		}
		if (registrationDto.getContact().matches("[789]{1}[0-9]{9}") != true) {
			errors.rejectValue("contact", "error.contact.first.rule");
		}
	}

	
}
